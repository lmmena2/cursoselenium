import unittest
from pyunitreport import HTMLTestRunner
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException


class SearchTest(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path = r'C://Users/Usuario/Desktop/cmder/chromedriver_win32/chromedriver.exe')
        driver = self.driver
        driver.get('http://demo-store.seleniumacademy.com/') 
        driver.maximize_window()
        driver.implicitly_wait(30)
    
    def test_search_tee(self):
        driver = self.driver
        search_field = driver.find_element(By.NAME, 'q')
        search_field.clear()
        
        search_field.send_keys('tee')
        search_field.submit()
        
    def test_search_salt_shaker(self):
        driver = self.driver
        search_field = driver.find_element(By.NAME,'q')
        
        search_field.send_keys('salt shaker')
        search_field.submit()
        
        products = driver.find_element(By.XPATH,'//*[@id="product-collection-image-389"]')
        self.assertEqual(1, len(products))
        
    def tearDown(self):
        self.driver.quit()